/**
 * Flow Interoperability Project (flowinterop.org)
 * Flow Runner
 * Copyright (c) 2019, 2020 Viamo Inc.
 * Authored by: Brett Zabos (brett.zabos@viamo.io)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

// import UUID32 from "../model/UUID32";
import IBlock from './IBlock'
import {find} from 'lodash'
import ValidationException from '../domain/exceptions/ValidationException'
import SupportedMode from './SupportedMode'
import ILanguage from './ILanguage'

export interface IFlow {
  uuid: string // UUID32
  orgId: string
  name: string
  label?: string
  lastModified: string // UTC like: 2016-12-25 13:42:05.234598
  interactionTimeout: number
  platformMetadata: object

  supportedModes: SupportedMode[]
  languages: ILanguage[] // eunm for ISO 639-3 codes
  blocks: IBlock[]

  firstBlockId: string
  exitBlockId?: string
}

export default IFlow

export function findBlockWith(uuid: string, {blocks}: IFlow): IBlock {
  const block = find(blocks, {uuid})
  if (block == null) {
    throw new ValidationException('Unable to find block on flow')
  }

  return block
}

export interface IFlowService {
  findBlockWith(uuid: string, flow: IFlow): IBlock
}
