import FlowRunner from '../../src/domain/FlowRunner'
import IContext, {IReversibleUpdateOperation} from '../../src/flow-spec/IContext'
import IFlowRunner from '../../src/domain/IFlowRunner'
import IBlockInteraction from '../../src/flow-spec/IBlockInteraction'

describe('applyReversibleDataOperation', () => {
  let runner: IFlowRunner
  let context: IContext
  let operation: IReversibleUpdateOperation

  beforeEach(() => {
    context = {
      interactions: [] as IBlockInteraction[],
      sessionVars: {},
      reversibleOperations: [] as IReversibleUpdateOperation[]} as IContext
    runner = new FlowRunner(context)
    operation = {
      forward: {$set: {'sampleKey.sampleNestedKey': 'sample forward val'}},
      reverse: {$set: {'sampleKey.sampleNestedKey': 'sample reverse val'}},
    }
  })

  it('should store the transaction on context', () => {
    expect(context.reversibleOperations).toHaveLength(0)
    runner.applyReversibleDataOperation(operation.forward, operation.reverse, context)
    expect(context).toHaveProperty('reversibleOperations.0.forward', operation.forward)
    expect(context).toHaveProperty('reversibleOperations.0.reverse', operation.reverse)
  })

  it('should populate a interactionId that this operation was executed as a part of', () => {
    context.interactions.push({uuid: 'intx-123'} as IBlockInteraction)

    expect(context.reversibleOperations).toHaveLength(0)
    runner.applyReversibleDataOperation(operation.forward, operation.reverse, context)
    expect(context).toHaveProperty('reversibleOperations.0.interactionId', 'intx-123')
  })

  it('should apply the forward operation', () => {
    expect(context.sessionVars).not.toHaveProperty('sampleKey.sampleNestedKey')
    runner.applyReversibleDataOperation(operation.forward, operation.reverse, context)
    expect(context.sessionVars).toHaveProperty('sampleKey.sampleNestedKey', 'sample forward val')
  })

  it('should not apply the reversal operation', () => {
    expect(context.sessionVars).not.toHaveProperty('sampleKey.sampleNestedKey')
    runner.applyReversibleDataOperation(operation.forward, operation.reverse, context)
    expect(context.sessionVars).not.toHaveProperty('sampleKey.sampleNestedKey', 'sample reverse val')
  })
})
