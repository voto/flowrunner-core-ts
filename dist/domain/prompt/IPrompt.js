"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var KnownPrompts;
(function (KnownPrompts) {
    KnownPrompts["Message"] = "Message";
    KnownPrompts["Numeric"] = "Numeric";
    KnownPrompts["SelectOne"] = "SelectOne";
    KnownPrompts["SelectMany"] = "SelectMany";
    KnownPrompts["Open"] = "Open";
})(KnownPrompts = exports.KnownPrompts || (exports.KnownPrompts = {}));
//# sourceMappingURL=IPrompt.js.map