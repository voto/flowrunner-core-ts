export default interface IIdGenerator {
    generate(): string;
}
//# sourceMappingURL=IIdGenerator.d.ts.map