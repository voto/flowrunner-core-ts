"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const lodash_1 = require("lodash");
const IBlock_1 = require("../flow-spec/IBlock");
const IContext_1 = require("../flow-spec/IContext");
const lodash_2 = require("lodash");
const IdGeneratorUuidV4_1 = tslib_1.__importDefault(require("./IdGeneratorUuidV4"));
const ValidationException_1 = tslib_1.__importDefault(require("./exceptions/ValidationException"));
const IPrompt_1 = require("./prompt/IPrompt");
const MessagePrompt_1 = tslib_1.__importDefault(require("./prompt/MessagePrompt"));
const DeliveryStatus_1 = tslib_1.__importDefault(require("../flow-spec/DeliveryStatus"));
const NumericPrompt_1 = tslib_1.__importDefault(require("./prompt/NumericPrompt"));
const OpenPrompt_1 = tslib_1.__importDefault(require("./prompt/OpenPrompt"));
const SelectOnePrompt_1 = tslib_1.__importDefault(require("./prompt/SelectOnePrompt"));
const SelectManyPrompt_1 = tslib_1.__importDefault(require("./prompt/SelectManyPrompt"));
const BasicBacktrackingBehaviour_1 = tslib_1.__importDefault(require("./behaviours/BacktrackingBehaviour/BasicBacktrackingBehaviour"));
const MessageBlockRunner_1 = tslib_1.__importDefault(require("./runners/MessageBlockRunner"));
const OpenResponseBlockRunner_1 = tslib_1.__importDefault(require("./runners/OpenResponseBlockRunner"));
const NumericResponseBlockRunner_1 = tslib_1.__importDefault(require("./runners/NumericResponseBlockRunner"));
const SelectOneResponseBlockRunner_1 = tslib_1.__importDefault(require("./runners/SelectOneResponseBlockRunner"));
const SelectManyResponseBlockRunner_1 = tslib_1.__importDefault(require("./runners/SelectManyResponseBlockRunner"));
const CaseBlockRunner_1 = tslib_1.__importDefault(require("./runners/CaseBlockRunner"));
class BlockRunnerFactoryStore extends Map {
}
exports.BlockRunnerFactoryStore = BlockRunnerFactoryStore;
const DEFAULT_BEHAVIOUR_TYPES = [
    BasicBacktrackingBehaviour_1.default,
];
exports.NON_INTERACTIVE_BLOCK_TYPES = [
    'Core\\Case',
    'Core\\RunFlowBlock',
];
function createDefaultBlockRunnerStore() {
    return new BlockRunnerFactoryStore([
        ['MobilePrimitives\\Message', (block, innerContext) => new MessageBlockRunner_1.default(block, innerContext)],
        ['MobilePrimitives\\OpenResponse', (block, innerContext) => new OpenResponseBlockRunner_1.default(block, innerContext)],
        ['MobilePrimitives\\NumericResponse', (block, innerContext) => new NumericResponseBlockRunner_1.default(block, innerContext)],
        ['MobilePrimitives\\SelectOneResponse', (block, innerContext) => new SelectOneResponseBlockRunner_1.default(block, innerContext)],
        ['MobilePrimitives\\SelectManyResponse', (block, innerContext) => new SelectManyResponseBlockRunner_1.default(block, innerContext)],
        ['Core\\Case', (block, innerContext) => new CaseBlockRunner_1.default(block, innerContext)]
    ]);
}
exports.createDefaultBlockRunnerStore = createDefaultBlockRunnerStore;
class FlowRunner {
    constructor(context, runnerFactoryStore = createDefaultBlockRunnerStore(), idGenerator = new IdGeneratorUuidV4_1.default, behaviours = {}) {
        this.context = context;
        this.runnerFactoryStore = runnerFactoryStore;
        this.idGenerator = idGenerator;
        this.behaviours = behaviours;
        this.initializeBehaviours(DEFAULT_BEHAVIOUR_TYPES);
    }
    initializeBehaviours(behaviourConstructors) {
        behaviourConstructors.forEach(b => this.behaviours[lodash_1.lowerFirst(lodash_1.trimEnd(b.name, 'Behaviour|Behavior'))]
            = new b(this.context, this, this));
    }
    initialize() {
        const ctx = this.context;
        const block = this.findNextBlockOnActiveFlowFor(ctx);
        if (block == null) {
            throw new ValidationException_1.default('Unable to initialize flow without blocks.');
        }
        ctx.deliveryStatus = DeliveryStatus_1.default.IN_PROGRESS;
        ctx.entryAt = (new Date).toISOString().replace('T', ' ');
        return this.navigateTo(block, this.context);
    }
    isInitialized(ctx) {
        return ctx.cursor != null;
    }
    isFirst() {
        const { cursor, interactions } = this.context;
        if (!this.isInitialized(this.context)) {
            return true;
        }
        const firstInteractiveIntx = lodash_2.find(interactions, ({ type }) => !lodash_2.includes(exports.NON_INTERACTIVE_BLOCK_TYPES, type));
        if (firstInteractiveIntx == null) {
            return true;
        }
        return firstInteractiveIntx.uuid === cursor[0];
    }
    isLast() {
        const { cursor, interactions } = this.context;
        if (!this.isInitialized(this.context)) {
            return true;
        }
        return lodash_2.last(interactions).uuid === cursor[0];
    }
    run() {
        const { context: ctx } = this;
        if (!this.isInitialized(ctx)) {
            this.initialize();
        }
        return this.runUntilInputRequiredFrom(ctx);
    }
    isInputRequiredFor(ctx) {
        return ctx.cursor != null
            && ctx.cursor[1] != null
            && ctx.cursor[1].value === undefined;
    }
    runUntilInputRequiredFrom(ctx) {
        let richCursor = this.hydrateRichCursorFrom(ctx);
        let block = IContext_1.findBlockOnActiveFlowWith(richCursor[0].blockId, ctx);
        do {
            if (this.isInputRequiredFor(ctx)) {
                console.info('Attempted to resume when prompt is not yet fulfilled; resurfacing same prompt instance.');
                return richCursor;
            }
            this.runActiveBlockOn(richCursor, block);
            block = this.findNextBlockOnActiveFlowFor(ctx);
            if (block == null) {
                block = this.stepOut(ctx);
            }
            if (block == null) {
                continue;
            }
            if (block.type === 'Core\\RunFlowBlock') {
                richCursor = this.navigateTo(block, ctx);
                block = this.stepInto(block, ctx);
            }
            if (block == null) {
                continue;
            }
            richCursor = this.navigateTo(block, ctx);
        } while (block != null);
        this.complete(ctx);
        return;
    }
    complete(ctx) {
        lodash_2.last(ctx.interactions).exitAt = (new Date).toISOString().replace('T', ' ');
        delete ctx.cursor;
        ctx.deliveryStatus = DeliveryStatus_1.default.FINISHED_COMPLETE;
        ctx.exitAt = (new Date).toISOString().replace('T', ' ');
    }
    dehydrateCursor(richCursor) {
        return [richCursor[0].uuid, richCursor[1] != null ? richCursor[1].config : undefined];
    }
    hydrateRichCursorFrom(ctx) {
        const { cursor } = ctx;
        const interaction = IContext_1.findInteractionWith(cursor[0], ctx);
        return [interaction, this.createPromptFrom(cursor[1], interaction)];
    }
    initializeOneBlock(block, flowId, originFlowId, originBlockInteractionId) {
        let interaction = this.createBlockInteractionFor(block, flowId, originFlowId, originBlockInteractionId);
        Object.values(this.behaviours)
            .forEach(b => interaction = b.postInteractionCreate(interaction, this.context));
        return [interaction, this.buildPromptFor(block, interaction)];
    }
    runActiveBlockOn(richCursor, block) {
        if (richCursor[1] != null) {
            richCursor[0].value = richCursor[1].value;
            richCursor[0].hasResponse = true;
        }
        const exit = this.createBlockRunnerFor(block, this.context)
            .run(richCursor);
        richCursor[0].selectedExitId = exit.uuid;
        if (richCursor[1] != null) {
            richCursor[1].config.isSubmitted = true;
        }
        Object.values(this.behaviours)
            .forEach(b => b.postInteractionComplete(richCursor[0], this.context));
        return exit;
    }
    createBlockRunnerFor(block, ctx) {
        const factory = this.runnerFactoryStore.get(block.type);
        if (factory == null) {
            throw new ValidationException_1.default(`Unable to find factory for block type: ${block.type}`);
        }
        return factory(block, ctx);
    }
    navigateTo(block, ctx, navigatedAt = new Date) {
        const { interactions, nestedFlowBlockInteractionIdStack } = ctx;
        const flowId = IContext_1.getActiveFlowIdFrom(ctx);
        const originInteractionId = lodash_2.last(nestedFlowBlockInteractionIdStack);
        const originInteraction = originInteractionId != null
            ? IContext_1.findInteractionWith(originInteractionId, ctx)
            : null;
        const richCursor = this.initializeOneBlock(block, flowId, originInteraction == null ? undefined : originInteraction.flowId, originInteractionId);
        const lastInteraction = lodash_2.last(interactions);
        if (lastInteraction != null) {
            lastInteraction.exitAt = navigatedAt.toISOString().replace('T', ' ');
        }
        interactions.push(richCursor[0]);
        ctx.cursor = this.dehydrateCursor(richCursor);
        return richCursor;
    }
    stepInto(runFlowBlock, ctx) {
        if (runFlowBlock.type !== 'Core\\RunFlow') {
            throw new ValidationException_1.default('Unable to step into a non-Core\\RunFlow block type');
        }
        const runFlowInteraction = lodash_2.last(ctx.interactions);
        if (runFlowInteraction == null) {
            throw new ValidationException_1.default('Unable to step into Core\\RunFlow that hasn\'t yet been started');
        }
        if (runFlowBlock.uuid !== runFlowInteraction.blockId) {
            throw new ValidationException_1.default('Unable to step into Core\\RunFlow block that doesn\'t match last interaction');
        }
        ctx.nestedFlowBlockInteractionIdStack.push(runFlowInteraction.uuid);
        const firstNestedBlock = lodash_2.first(IContext_1.getActiveFlowFrom(ctx).blocks);
        if (firstNestedBlock == null) {
            return undefined;
        }
        runFlowInteraction.selectedExitId = runFlowBlock.exits[0].uuid;
        return firstNestedBlock;
    }
    stepOut(ctx) {
        const { nestedFlowBlockInteractionIdStack } = ctx;
        if (nestedFlowBlockInteractionIdStack.length === 0) {
            return;
        }
        const lastParentInteractionId = nestedFlowBlockInteractionIdStack.pop();
        const { blockId: lastRunFlowBlockId } = IContext_1.findInteractionWith(lastParentInteractionId, ctx);
        const lastRunFlowBlock = IContext_1.findBlockOnActiveFlowWith(lastRunFlowBlockId, ctx);
        const { destinationBlock } = lodash_2.first(lastRunFlowBlock.exits);
        if (destinationBlock == null) {
            return;
        }
        return IContext_1.findBlockOnActiveFlowWith(destinationBlock, ctx);
    }
    findNextBlockOnActiveFlowFor(ctx) {
        const flow = IContext_1.getActiveFlowFrom(ctx);
        const { cursor } = ctx;
        if (cursor == null) {
            return lodash_2.first(flow.blocks);
        }
        const interaction = IContext_1.findInteractionWith(cursor[0], ctx);
        return this.findNextBlockFrom(interaction, ctx);
    }
    findNextBlockFrom(interaction, ctx) {
        if (interaction.selectedExitId == null) {
            throw new ValidationException_1.default('Unable to navigate past incomplete interaction; did you forget to call runner.run()?');
        }
        const block = IContext_1.findBlockOnActiveFlowWith(interaction.blockId, ctx);
        const { destinationBlock } = IBlock_1.findBlockExitWith(interaction.selectedExitId, block);
        const { blocks } = IContext_1.getActiveFlowFrom(ctx);
        return lodash_2.find(blocks, { uuid: destinationBlock });
    }
    createBlockInteractionFor({ uuid: blockId, type }, flowId, originFlowId, originBlockInteractionId) {
        return {
            uuid: this.idGenerator.generate(),
            blockId,
            flowId,
            entryAt: (new Date).toISOString().replace('T', ' '),
            exitAt: undefined,
            hasResponse: false,
            value: undefined,
            selectedExitId: null,
            details: {},
            type,
            originFlowId,
            originBlockInteractionId,
        };
    }
    buildPromptFor(block, interaction) {
        const runner = this.createBlockRunnerFor(block, this.context);
        const promptConfig = runner.initialize(interaction);
        return this.createPromptFrom(promptConfig, interaction);
    }
    createPromptFrom(config, interaction) {
        if (config == null || interaction == null) {
            return;
        }
        const kindConstructor = {
            [IPrompt_1.KnownPrompts.Message]: MessagePrompt_1.default,
            [IPrompt_1.KnownPrompts.Numeric]: NumericPrompt_1.default,
            [IPrompt_1.KnownPrompts.Open]: OpenPrompt_1.default,
            [IPrompt_1.KnownPrompts.SelectOne]: SelectOnePrompt_1.default,
            [IPrompt_1.KnownPrompts.SelectMany]: SelectManyPrompt_1.default,
        }[config.kind];
        return new kindConstructor(config, interaction.uuid, this);
    }
}
exports.default = FlowRunner;
//# sourceMappingURL=FlowRunner.js.map