export default interface IContact {
    id: string;
    name: string;
    createdAt: string;
}
//# sourceMappingURL=IContact.d.ts.map