export declare enum SupportedMode {
    SMS = "sms",
    USSD = "ussd",
    IVR = "ivr",
    RICH_MESSAGING = "rich_messaging",
    OFFLINE = "offline"
}
export default SupportedMode;
//# sourceMappingURL=SupportedMode.d.ts.map