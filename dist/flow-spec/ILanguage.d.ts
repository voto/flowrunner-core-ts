export default interface ILanguage {
    id: string;
    name: string;
    abbreviation: string;
    orgId: string;
    rightToLeft: boolean;
    code?: string;
    deletedAt?: string;
}
//# sourceMappingURL=ILanguage.d.ts.map