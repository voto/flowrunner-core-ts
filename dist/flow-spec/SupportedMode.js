"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SupportedMode;
(function (SupportedMode) {
    SupportedMode["SMS"] = "sms";
    SupportedMode["USSD"] = "ussd";
    SupportedMode["IVR"] = "ivr";
    SupportedMode["RICH_MESSAGING"] = "rich_messaging";
    SupportedMode["OFFLINE"] = "offline";
})(SupportedMode = exports.SupportedMode || (exports.SupportedMode = {}));
exports.default = SupportedMode;
//# sourceMappingURL=SupportedMode.js.map