export default interface INumericBlockConfig {
    prompt: string;
    promptAudio: string;
    validationMinimum: number;
    validationMaximum: number;
    ivr: {
        maxDigits: number;
    };
}
//# sourceMappingURL=INumericBlockConfig.d.ts.map