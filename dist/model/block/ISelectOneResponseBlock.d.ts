import { IBlockWithTestExits } from '../../flow-spec/IBlock';
import ISelectOneResponseBlockConfig from './ISelectOneResponseBlockConfig';
export default interface ISelectOneResponseBlock extends IBlockWithTestExits {
    config: ISelectOneResponseBlockConfig;
}
//# sourceMappingURL=ISelectOneResponseBlock.d.ts.map