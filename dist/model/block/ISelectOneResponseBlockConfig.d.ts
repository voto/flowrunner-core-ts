export default interface ISelectOneResponseBlockConfig {
    prompt: string;
    promptAudio: string;
    questionPrompt?: string;
    choicesPrompt?: string;
    choices: StringMapType;
}
declare type StringMapType = {
    [k: string]: string;
};
export {};
//# sourceMappingURL=ISelectOneResponseBlockConfig.d.ts.map